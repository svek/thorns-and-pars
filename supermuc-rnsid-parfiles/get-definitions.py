#!/usr/bin/env python
# SuperMUC: module load python before.
# -- Sven, 20. Jun 2016 on SuperMUC
# Reading LORENE CSV output with Pandas and creating Simfactory
# variable definition output.

#
# example use:
#
# python get-definitions.py jun20-tori.par 'enth==0.38 and freq == 380'
#
#

from __future__ import print_function
import sys
import re
import pandas as pd
from numpy import transpose

def log(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

run8 = pd.read_table("run8-table.txt", comment='#', delim_whitespace=True)

# use this to look which variables we have to declare:
# cat jun20-tori.par | grep @

try:
	parfile = sys.argv[1]
	query = sys.argv[2]
except :
	log("Usage: %s <parfile> <pandas-query>" % sys.argv[0])
	log("Example: %s torirnsid.par 'enth == 0.8 and freq == 0.3'" % sys.argv[0])
	sys.exit(-1)

#RNSID::log_enth_center = @log_enth_center@
#RNSID::axes_ratio = @axes_ratio@
#Whisky_ToriID::el0 = @tori_el0@
#Whisky_ToriID::delta = @tori_delta@
#Whisky_ToriID::Mass_grav = @Mass_grav@

parfile_content = "".join(list(open(parfile)))
variables = re.findall(r'@(star_(.+))@', parfile_content)
simvars, pdvars = transpose(variables)

log("Replacing @variables@ in ", pdvars)

data = run8.query(query)[pdvars]

log("Resulting stars of query: ")
log(data)

row2sim = lambda row: " ".join(["--define {0} {1}".format(simvar, row[pdvar])  for simvar, pdvar in variables])

for i, row in data.iterrows():
	print(row2sim(row))



