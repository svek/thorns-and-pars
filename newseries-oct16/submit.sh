#!/bin/bash

# expect parameter $1 to be @pert_factor@

partpl="oct05-collapse.par"
parsubmit="oct05-collapse-submit.par"

# simfactory parameters
simpars="--configuration somerville5 --walltime 10:00:00 --procs $((16*100))"

# checkpoint copying
simbase="$HOME/simulations"
chksim="sep26-evolve"
checkpoint="28672"

pert_factor="$1"
echo "TPL replace $partpl -> $parsubmit with '$pert_factor'"
sed "s/@pert_factor@/$pert_factor/" $partpl > $parsubmit

simname="oct05-collapse-k$pert_factor"
echo "Creating $simname"
sim create $simname --parfile $PWD/$parsubmit $simpars

rm $parsubmit

# do the checkpointing
cptarget="$HOME/simulations/$simname/checkpoint"
mkdir -p $cptarget

# copy stuff, goes very quickly.
cp -av $HOME/simulations/sep26-evolve/checkpoint/checkpoint.chkpt.it_${checkpoint}* $cptarget/

echo "done copying checkpoints. Submitting"

sim submit $simname $simpars
