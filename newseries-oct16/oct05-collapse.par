# Oct 05: Now doing the collapse.

# This parfile is based on the jul14-refined.par.
# There were problems on x-men and also supermuc.
#
# This parfile shall serve as basis for the new approach to the collapse by
# modifying `dens` and that's it.

# started jul14, 10:33:
# based on p2c-thc.par on X-MEN which came from the ccz3-noreflux-tori3.92 on supermuc.

# New generation of Whisky_ToriID-RNSID parameter file possibly with
# checkpointing, rugutil compat, refluxing, CCZ4,
# whisky_analyis, lesser pizza dependence. Yay.
#
# -- Sven, 30. Jun 2016

Cactus::cctk_run_title = "TORI-CCZ4-RNSID"
Cactus::cctk_full_warnings         = no
Cactus::highlight_warning_messages = "yes"
Cactus::terminate = "never"

ActiveThorns = "TerminationTrigger"
#TerminationTrigger::max_walltime = 5000.0
TerminationTrigger::max_walltime = @WALLTIME_HOURS@
#TerminationTrigger::on_remaining_walltime  = 30
TerminationTrigger::create_termination_file = "yes"
TerminationTrigger::termination_from_file = "yes"
TerminationTrigger::termination_file = "TERMINATE"


ActiveThorns = "IOUtil"
ActiveThorns = "AEILocalInterp"
ActiveThorns = "Constants"
ActiveThorns = "Fortran"
ActiveThorns = "GSL"
ActiveThorns = "GenericFD"
ActiveThorns = "HDF5"
ActiveThorns = "LoopControl"
ActiveThorns = "CarpetIOASCII CarpetIOScalar CarpetIOHDF5 CarpetIOBasic"
ActiveThorns = "hydrobase InitBase"
ActiveThorns = "Constants NaNChecker ReflectionSymmetry RotatingSymmetry90"
ActiveThorns = "InitBase Slab" # carpetslab instead?
ActiveThorns = "Carpet CarpetLib CarpetInterp CarpetReduce"

Carpet::domain_from_coordbase = yes
#Carpet::refinement_centering = "cell" # needed for Refluxing
Driver::ghost_size       = 3
Carpet::use_buffer_zones = yes

Carpet::prolongation_order_space = 5
Carpet::prolongation_order_time  = 2
Carpet::convergence_level = 0
Carpet::init_fill_timelevels = yes
Carpet::poison_new_timelevels = yes
CarpetLib::poison_new_memory  = yes
Carpet::output_timers_every      = 5120
CarpetLib::print_timestats_every = 5120
CarpetLib::print_memstats_every  = 5120


ActiveThorns = "NaNChecker"

NaNChecker::check_every     = 512
NaNChecker::verbose         = "standard"
NaNChecker::action_if_found = "abort"
NaNChecker::check_vars      = "
	ADMBase::lapse
	ADMBase::shift
	HydroBase::w_lorentz
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        ADMBase::dtlapse
        ADMBase::dtshift
        HydroBase::rho
        HydroBase::press
        HydroBase::eps
        HydroBase::vel
"


# =============================================================================
# Grid
# =============================================================================

ActiveThorns = "Boundary CartGrid3D CoordBase SymBase"
CoordBase::xmin =    0.00
CoordBase::ymin =    0.00
CoordBase::zmin =    0.00
CoordBase::xmax = 240.00
CoordBase::ymax = 240.00
CoordBase::zmax = 120.00

CoordBase::dx   =    2.4
CoordBase::dy   =    2.4
CoordBase::dz   =    1.2

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

# this is needed for refluxing
CoordBase::boundary_shiftout_x_lower = 1
CoordBase::boundary_shiftout_y_lower = 1
CoordBase::boundary_shiftout_z_lower = 1
#CoordBase::boundary_shiftout_x_upper = 0
#CoordBase::boundary_shiftout_y_upper = 0
#CoordBase::boundary_shiftout_z_upper = 0

# this is needed for refluxing
#CoordBase::boundary_staggered_x_lower = "yes"
#CoordBase::boundary_staggered_x_upper = "yes"
#CoordBase::boundary_staggered_y_lower = "yes"
#CoordBase::boundary_staggered_y_upper = "yes"
#CoordBase::boundary_staggered_z_lower = "yes"
#CoordBase::boundary_staggered_z_upper = "yes"

CartGrid3D::type = "coordbase"
CartGrid3D::domain = "full"
CartGrid3D::avoid_origin = "no"
#CartGrid3D::rotation_axis="z"
#CartGrid3D::bitant_plane="xz"

# there is also the thorn RotatingSymmetry90 active:
ReflectionSymmetry::reflection_z = "yes"
ReflectionSymmetry::avoid_origin_z = "no"

ActiveThorns = "Volomnia"
Volomnia::symm_weight = 4

ActiveThorns = "SphericalSurface"

ActiveThorns = "CarpetRegrid2"
Carpet::max_refinement_levels    = 3
CarpetRegrid2::regrid_every      = 0
CarpetRegrid2::num_centres       = 1
CarpetRegrid2::num_levels_1      = 3
CarpetRegrid2::snap_to_coarse    = "yes" # for Refluxing

CarpetRegrid2::radius_1[1]       = 30.0 # dx=0.15
CarpetRegrid2::radius_1[2]       = 60.0 # dx=0.3
CarpetRegrid2::radius_1[3]       = 120.0 # dx=0.6

Carpet::grid_coordinates_filename = "grid.carpet"

# Time intergration
ActiveThorns = "MoL Time"

MoL::ODE_Method             = "RK3"
MoL::MoL_Intermediate_Steps = 3
MoL::MoL_Num_Scratch_Levels = 2

Time::dtfac = 0.2

ActiveThorns = "ADMBase ADMCoupling ADMMacros CoordGauge SpaceMask StaticConformal"
ActiveThorns = "HydroBase TmunuBase EOS_Base"

ADMMacros::spatial_order = 4

HydroBase::timelevels        = 3
HydroBase::prolongation_type = "ENO"

TmunuBase::stress_energy_storage = yes
TmunuBase::stress_energy_at_RHS  = yes
TmunuBase::timelevels            = 1
TmunuBase::prolongation_type     = "none"

SpaceMask::use_mask = yes

# Numerical evolution scheme: CCZ4
ActiveThorns = "ML_CCZ4 ML_CCZ4_Helper NewRad Dissipation"

ADMBase::evolution_method         = "ML_CCZ4"
ADMBase::lapse_evolution_method   = "ML_CCZ4"
ADMBase::shift_evolution_method   = "ML_CCZ4"
ADMBase::dtlapse_evolution_method = "ML_CCZ4"
ADMBase::dtshift_evolution_method = "ML_CCZ4"


ML_CCZ4::GammaShift             = 0.5
ML_CCZ4::dampk1                 = 0.036  # ~ [0.02, 0.1]/M
ML_CCZ4::dampk2                 = 0.0
ML_CCZ4::harmonicN              = 1.0    # 1+log
ML_CCZ4::harmonicF              = 2.0    # 1+log
ML_CCZ4::ShiftGammaCoeff        = 0.75
ML_CCZ4::AlphaDriver            = 0.0
ML_CCZ4::BetaDriver             = 0.3    # ~ [1, 2] / M (\eta)

ML_CCZ4::advectLapse            = 1
ML_CCZ4::advectShift            = 1

ML_CCZ4::MinimumLapse           = 1.0e-8
ML_CCZ4::conformalMethod        = 1      # 1 for W

ML_CCZ4::initial_boundary_condition  = "extrapolate-gammas"
ML_CCZ4::rhs_boundary_condition      = "NewRad"
Boundary::radpower                   = 2

ML_CCZ4::ML_log_confac_bound    = "none"
ML_CCZ4::ML_metric_bound        = "none"
ML_CCZ4::ML_Gamma_bound         = "none"
ML_CCZ4::ML_trace_curv_bound    = "none"
ML_CCZ4::ML_curv_bound          = "none"
ML_CCZ4::ML_lapse_bound         = "none"
ML_CCZ4::ML_dtlapse_bound       = "none"
ML_CCZ4::ML_shift_bound         = "none"
ML_CCZ4::ML_dtshift_bound       = "none"
ML_CCZ4::ML_Theta_bound         = "none"

ML_CCZ4::fdOrder                = 4
THC_Core::fd_order              = 4


Dissipation::order                      = 5
Dissipation::epsdis                     = 0.2
Dissipation::vars                       = "
ML_CCZ4::ML_log_confac
ML_CCZ4::ML_metric
ML_CCZ4::ML_trace_curv
ML_CCZ4::ML_curv
ML_CCZ4::ML_Gamma
ML_CCZ4::ML_lapse
ML_CCZ4::ML_shift
ML_CCZ4::ML_dtlapse
ML_CCZ4::ML_dtshift
ML_CCZ4::ML_Theta
"

# Templated hydrodynamics code
ActiveThorns = "THC_Core THC_Tracer EOS_Thermal EOS_Thermal_Idealgas"
HydroBase::evolution_method		= "THCode"
THC_Core::eos_type			= "ideal"

# "has already been set to the same value before"
#TmunuBase::prolongation_type		= "none"
#TmunuBase::stress_energy_storage		= "yes"
#TmunuBase::stress_energy_at_RHS		= "yes"
#TmunuBase::support_old_CalcTmunu_mechanism	= "no"

THC_Core::bc_type			= "none"

# atmo should be set down to e-15 at some time.
THC_Core::atmo_rho			= 1e-15
THC_Core::atmo_eps			= 1e-8

EOS_Thermal::evol_eos_name		= "IdealGas"
EOS_Thermal_IdealGas::index_n		= 1

EOS_Thermal_IdealGas::eps_max  = 2

# THC Refluxing
#ActiveThorns = "Refluxing THC_Refluxing HRSCCore"
#THC_Refluxing::nvars = 5 # expected for ideal EOS
#
# this is needed for THC Refluxing
#HRSCCore::scheme          = "FV"
#HRSCCore::pplim           = "yes"
#HRSCCore::reconstruction  = "MP5"
#HRSCCore::riemann_solver  = "HLLE"
#HRSCCore::refluxing       = "yes"
#HRSCCore::flux_split      = "LLF"
#HRSCCore::system_split    = "components"
#HRSCCore::speed_eps       = 0.05

ActiveThorns = "ML_ADMConstraints"

#ADMConstraints::constraints_persist    = yes
#ADMConstraints::constraints_timelevels = 3


# =============================================================================
# Initial data
# =============================================================================
ActiveThorns = "PizzaBase PizzaIDBase PizzaNumUtils"
ActiveThorns = "RNSID"

# RNSID at this point
##  #ADMBase::initial_data    = "one" # rnsid"
##ADMBase::initial_lapse   = "one" # rnsid"
##ADMBase::initial_shift   = "zero" # rnsid"
##ADMBase::initial_dtlapse = "zero"
##ADMBase::initial_dtshift = "zero"
##HydroBase::initial_hydro = "rnsid"

ADMBase::initial_data    = "rnsid"
ADMBase::initial_lapse   = "rnsid"
ADMBase::initial_shift   = "rnsid"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"
HydroBase::initial_hydro = "rnsid"

# This is identical to the LORENE star run7/star-enth0.251724-freq520.000000
# ie. f = 520 Hz with the new polytrope for this hydromeeting.
#RNSID::rho_center = 0.00115748174546342
RNSID::rho_center = -1
RNSID::log_enth_center = 0.38
#RNSID::axes_ratio = 0.8734803165
RNSID::axes_ratio = 0.9598953
RNSID::rotation_type = "uniform"

# (almost, still trying out)
# relict because RNSID does not yet use PizzaEOS infrastructure
RNSID::RNS_Gamma = 2
RNSID::RNS_K = 123.647

# there is still some redundancy/ambiguities in the atmo parameters
# keep in sync with THC_Core::atmo_rho.
RNSID::rho_cut = 1e-15
RNSID::whisky_atmo_tolerance = 1e-15

# EOS Pizza file used only for RNSID.
PizzaIDBase::eos_from = "Nowhere"

# Geometric unit system for initial data, specified by length unit.
# use CACTUS units
PizzaBase::length_unit		= 1476.7161818921163

ActiveThorns="Whisky_ToriID"
Whisky_ToriID::el0 = 3.92
Whisky_ToriID::delta = 0

# scale rmd, pressure, etc. with constant factor
Whisky_ToriID::rho_scale = 0.05

# EOS from the star. LoreneMNS-Polytrope(1,3.32e+18)
Whisky_ToriID::eos_k = 123.647
Whisky_ToriID::eos_gamma = 2
# Gravitational mass from the star in M_sol
# told by Lorene or RNSID
Whisky_ToriID::Mass_grav = 1.814414
# typical value: 1.710533076

#Whisky_ToriID::dbg_flag = 1
#

# Apply torus perturbation
ActiveThorns = "PerturbeEvolution"
PerturbeEvolution::at = "poststep"
PerturbeEvolution::iteration = 28690 
PerturbeEvolution::iteration_duration = 10
PerturbeEvolution::pressure_multiply = @pert_factor@
PerturbeEvolution::rho_treshold = 2.15278e-05
PerturbeEvolution::prim2con = "none"

# another output thorn
ActiveThorns = "Whisky_Analysis"
Whisky_Analysis::compute_angular_velocity = "yes"
Whisky_Analysis::compute_specific_angular_momentum = "yes"


# =============================================================================
# Output
# =============================================================================
IO::out_dir = "data/"
IOUtil::strict_io_parameter_check = "yes"
IOUtil::parfile_write = "copy"
IOBasic::outInfo_every = 1
IOBasic::outInfo_reductions = "maximum minimum"
IOBasic::outInfo_vars = "
        Carpet::physical_time_per_hour
        HydroBase::rho
        HydroBase::eps
        THC_Core::tau
        ADMBase::lapse
"

# Scalar information:
IOScalar::outScalar_vars      = "
	admbase::lapse
	admbase::shift
	admbase::curv
	admbase::metric
	hydrobase::rho
	hydrobase::press
	hydrobase::eps
	hydrobase::w_lorentz
	hydrobase::vel
	THC_Core::dens
	THC_Core::tau
	THC_Core::scon
	THC_Core::volform
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
	Whisky_Analysis::specific_angular_momentum
	Whisky_Analysis::angular_velocity
"

IOScalar::outScalar_criterion           = "divisor"
IOScalar::outScalar_every               = 1
IOScalar::outScalar_reductions          = "average minimum maximum norm1 norm2 sum"

# 0D ascii whatever
IOASCII::out0D_vars = "
	Carpet::physical_time_per_hour
	Volomnia::grid_volume
	Volomnia::cell_volume
	Carpet::timing
"

IOASCII::out0D_criterion = "divisor"
IOASCII::out0D_every = 80
IOASCII::out0D_dir = "data/timing/"

IOASCII::out1D_vars      = "
	ADMBase::lapse
	ADMBase::shift
	ADMBase::curv
	ADMBase::metric
	hydrobase::rho
	hydrobase::press
	hydrobase::eps
	hydrobase::w_lorentz
	hydrobase::vel
	THC_Core::scon
	THC_Core::dens
	THC_Core::tau
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
	Whisky_Analysis::specific_angular_momentum
	Whisky_Analysis::angular_velocity
"

IOASCII::out1D_criterion                = "divisor"
IOASCII::out1D_d                        = "no"
IOASCII::out1D_every                    = 10

IOHDF5::out1D_vars = "
        ADMBase::lapse
	ADMBase::shift
	ADMBase::curv
	ADMBase::metric
        hydrobase::rho
        hydrobase::press
        hydrobase::eps
        hydrobase::w_lorentz
        hydrobase::vel
	THC_Core::scon
        THC_Core::dens
        THC_Core::tau
	THC_Core::csound
	THC_Core::volform
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
	Whisky_Analysis::specific_angular_momentum
	Whisky_Analysis::angular_velocity
"

IOHDF5::out1D_criterion                 = "divisor"
IOHDF5::out1D_d                         = "no"
IOHDF5::out1D_every                     = 40

IOHDF5::out2D_vars = "
	ADMBase::lapse
	ADMBase::shift
	ADMBase::curv
	ADMBase::metric
        HydroBase::eps
        HydroBase::press
        HydroBase::rho
        HydroBase::vel
        HydroBase::w_lorentz
	THC_Core::dens
	THC_Core::scon
	THC_Core::tau
	THC_Core::csound
	THC_Core::volform
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
	Whisky_Analysis::specific_angular_momentum
	Whisky_Analysis::angular_velocity
"

IOHDF5::out2D_criterion                 = "divisor"
IOHDF5::out2D_every                     = 80
IOHDF5::output_index                    = "yes"


# To reduce the number of files in the output directory
IOScalar::all_reductions_in_one_file    = "yes"
IOScalar::one_file_per_group            = "yes"
IOASCII::one_file_per_group             = "yes"
IOHDF5::one_file_per_group              = "no"
IOHDF5::compression_level               = 7

# =============================================================================
# Checkpoint (as by /home/hpc/pr84fa/di25cux/homes/luke-bovard-di29huy/par_old )
# =============================================================================
IOHDF5::checkpoint			= "yes"
IOHDF5::use_reflevels_from_checkpoint   = "yes"
#IOHDF5::open_one_input_file_at_a_time  	= "yes"

IOUtil::checkpoint_on_terminate	= "yes"
IOUtil::checkpoint_every	= 2048
IOUtil::checkpoint_keep		= 2
IOUtil::recover			= "autoprobe"
IOUtil::checkpoint_dir		= "../checkpoint"
IOUtil::recover_dir		= "../checkpoint"


