###============================================================###
###                                                            ###
###   The purpus of this par file ::                           ###
###   In Whisyky,                                              ###
###   (1) Isolated Neutron Star  ( RNS )                       ###
###                                                            ###
###============================================================###

Cactus::cctk_run_title		= "Isolated Neutron Star"
Cactus::cctk_full_warnings	= "yes"

Cactus::terminate		= "iteration"
# was 5M
Cactus::cctk_itlast		= 5000000

###============================================================###
###   Activation of Thorns
###============================================================###

ActiveThorns = "Recovery_Util"
ActiveThorns = "Fortran"
ActiveThorns = "LocalInterp AEILocalInterp"
ActiveThorns = "Slab"
ActiveThorns = "IOUtil"
ActiveThorns = "InitBase Carpet CarpetLib CarpetInterp CarpetReduce LoopControl"
ActiveThorns = "NaNChecker"
ActiveThorns = "Boundary CartGrid3D CoordBase SymBase"
ActiveThorns = "SphericalSurface CarpetRegrid2"
ActiveThorns = "ADMBase ADMCoupling ADMMacros CoordGauge SpaceMask StaticConformal"
ActiveThorns = "Time"
ActiveThorns = "MoL"
ActiveThorns = "CarpetIOScalar CarpetIOASCII CarpetIOBasic CarpetIOHDF5"
ActiveThorns = "Dissipation"
ActiveThorns = "TimerReport Formaline"
ActiveThorns = "ReflectionSymmetry RotatingSymmetry90"
ActiveThorns = "HydroBase TmunuBase InitBase"
ActiveThorns = "EoS_Base EoS_Polytrope EoS_IdealFluid"
ActiveThorns = "Whisky Whisky_Analysis"
ActiveThorns = "GenericFD"
ActiveThorns = "ML_BSSN ML_BSSN_Helper ML_ADMConstraints NewRad"
ActiveThorns = "ManualTermination"


###============================================================###
###   INITIAL DATA
###============================================================###

ActiveThorns = "Whisky_RNSID"

ADMBase::metric_type		= "physical"
ADMBase::initial_data		= "rnsid"
ADMBase::initial_lapse		= "rnsid"
ADMBase::initial_shift		= "rnsid"
ADMBase::initial_dtlapse	= "zero"
ADMBase::initial_dtshift	= "zero"

Whisky::whisky_rho_central	= 0.0016188651096701446  # 1.0 x 10^{15} [g/cm^3]

rnsid::axes_ratio		= 0.8125  # 52.0/64.0
rnsid::RNS_Gamma		= 2.0
rnsid::RNS_K			= 123.647

rnsid::accuracy			= 1.0e-10
rnsid::MDIV			= 1001
rnsid::SDIV			= 1301

###   M_GRAV ~ ----- M_sun.
###   M_ADM  ~ ----- M_sun.

###==============================###

InitBase::initial_data_setup_method	= "init_all_levels"

MoL::initial_data_is_crap		= "yes"


###============================================================###
###   GRID SETUP
###============================================================###

Time::dtfac				= 0.30

Carpet::verbose				= "no"
Carpet::veryverbose			= "no"
Carpet::domain_from_coordbase		= "yes"

CartGrid3D::type			= "coordbase"
CartGrid3D::domain			= "full"

CartGrid3D::avoid_originx		= "no"
CartGrid3D::avoid_originy		= "no"
CartGrid3D::avoid_originz		= "no"

ReflectionSymmetry::reflection_x	= "no"
ReflectionSymmetry::reflection_y	= "no"
ReflectionSymmetry::reflection_z	= "yes"

ReflectionSymmetry::avoid_origin_x	= "no"
ReflectionSymmetry::avoid_origin_y	= "no"
ReflectionSymmetry::avoid_origin_z	= "no"

CarpetRegrid2::symmetry_rotating90	= "yes"

RotatingSymmetry90::verbose		= "no"

CoordBase::boundary_size_x_lower	= 3
CoordBase::boundary_size_y_lower	= 3
CoordBase::boundary_size_z_lower	= 3
CoordBase::boundary_size_x_upper	= 3
CoordBase::boundary_size_y_upper	= 3
CoordBase::boundary_size_z_upper	= 3

CoordBase::boundary_shiftout_x_lower	= 1
CoordBase::boundary_shiftout_y_lower	= 1
CoordBase::boundary_shiftout_z_lower	= 1

CoordBase::domainsize			= "minmax"
CoordBase::xmax				= 20.0
CoordBase::ymax				= 20.0
CoordBase::zmax				= 20.0
CoordBase::xmin				=  0.0
CoordBase::ymin				=  0.0
CoordBase::zmin				=  0.0
CoordBase::dx				=  0.5
CoordBase::dy				=  0.5
CoordBase::dz				=  0.5

Carpet::max_refinement_levels		= 2
CarpetRegrid2::num_centres		= 1

CarpetRegrid2::num_levels_1		= 2
CarpetRegrid2::position_x_1		=  0.0
CarpetRegrid2::position_y_1		=  0.0
CarpetRegrid2::position_z_1		=  0.0
CarpetRegrid2::radius_1[1]		= 12.0

Driver::ghost_size			= 3
Carpet::use_buffer_zones		= "yes"
Carpet::prolongation_order_space	= 3
Carpet::prolongation_order_time		= 2
Carpet::convergence_level		= 0

Carpet::enable_all_storage		= "no"

Carpet::regrid_in_level_mode		= "yes"
Carpet::regrid_during_initialisation	= "no"
Carpet::init_each_timelevel		= "no"

Whisky::compute_DCandMaxima		= "no"
Whisky::DCandMaxima_every		= 1

CarpetRegrid2::regrid_every		= 0
CarpetRegrid2::verbose			= "yes"


###============================================================###
###   Evolution
###============================================================###

MoL::ode_method				= "rk4"
MoL::MoL_Intermediate_Steps		= 4
MoL::MoL_Num_Scratch_Levels		= 1
Carpet::num_integrator_substeps		= 4

###============================================================###
###   ADM and BSSN settings
###============================================================###

###++++++++++++++++++++++++++++++++++++++++
###   
###++++++++++++++++++++++++++++++++++++++++

ADMMacros::spatial_order		= 4
ML_BSSN::fdOrder			= 4


###++++++++++++++++++++++++++++++++++++++++
###   Formulation
###++++++++++++++++++++++++++++++++++++++++

ADMBase::evolution_method		= "ML_BSSN"

ML_BSSN::timelevels			= 3

ML_BSSN::apply_dissipation		= "never"

ML_BSSN::dt_lapse_shift_method		= "noLapseShiftAdvection"

ML_BSSN::conformalMethod		= 1

ML_BSSN::my_initial_boundary_condition	= "extrapolate-gammas"
ML_BSSN::my_rhs_boundary_condition	= "NewRad"
ML_BSSN::ML_log_confac_bound		= "none"
ML_BSSN::ML_metric_bound		= "none"
ML_BSSN::ML_Gamma_bound			= "none"
ML_BSSN::ML_trace_curv_bound		= "none"
ML_BSSN::ML_curv_bound			= "none"
ML_BSSN::ML_lapse_bound			= "none"
ML_BSSN::ML_dtlapse_bound		= "none"
ML_BSSN::ML_shift_bound			= "none"
ML_BSSN::ML_dtshift_bound		= "none"

Boundary::radpower			= 3


###++++++++++++++++++++++++++++++++++++++++
###   Gauges ( lapse function )
###++++++++++++++++++++++++++++++++++++++++

ADMBase::lapse_evolution_method		= "ML_BSSN"
ADMBase::dtlapse_evolution_method	= "ML_BSSN"

ML_BSSN::harmonicN			= 1
ML_BSSN::harmonicF			= 2.0

ML_BSSN::LapseAdvectionCoeff		= 1.0
ML_BSSN::AlphaDriver			= 0.0

ML_BSSN::MinimumLapse			= 1.0e-10


###++++++++++++++++++++++++++++++++++++++++
###   Gauges ( shift vector )
###++++++++++++++++++++++++++++++++++++++++

ADMBase::shift_evolution_method		= "ML_BSSN"
ADMBase::dtshift_evolution_method	= "ML_BSSN"

ML_BSSN::ShiftGammaCoeff		= 0.75
ML_BSSN::harmonicShift			= 0

ML_BSSN::BetaDriver			= 1.0928961748633879

ML_BSSN::ShiftAdvectionCoeff		= 1.0

ML_BSSN::ShiftBCoeff			= 1.0

ML_BSSN::LapseACoeff			= 0.0


###++++++++++++++++++++++++++++++++++++++++
###   Constraints
###++++++++++++++++++++++++++++++++++++++++

ML_BSSN::ML_BSSN_constraints1_calc_every = 100000000
ML_BSSN::ML_BSSN_constraints2_calc_every = 100000000


###============================================================###
###   Whisky
###============================================================###

TmunuBase::stress_energy_storage		= "yes"
TmunuBase::stress_energy_at_RHS			= "yes"
TmunuBase::timelevels				= 1
TmunuBase::prolongation_type			= "none"
TmunuBase::support_old_CalcTmunu_mechanism	= "no"

HydroBase::timelevels			= 3
HydroBase::evolution_method		= "whisky"
HydroBase::prolongation_type		= "ENO"

Whisky::riemann_solver			= "HLLE"
Whisky::hlle_type			= "Standard"

Whisky::recon_method			= "ppm"
Whisky::ppm_detect			= "yes"
Whisky::ppm_omega2			= 0.0
Whisky::ppm_flatten			= "stencil_3"

Whisky::whisky_stencil			= 3
Whisky::bound				= "none"
SpaceMask::use_mask			= "yes"

Whisky::whisky_c2p_warnlevel		= 0
Whisky::whisky_c2p_warn_from_reflevel	= 0
Whisky::c2p_reset_pressure		= "yes"
Whisky::c2p_reset_pressure_to_value	= 1.0e-18

Whisky::Whisky_Reset_NaNbyAtmo_at_RL	= 0

# these parameters are (no more?) available in Whisky
# Whisky::Whisky_MaxNumEvolvedVars	= 5
# Whisky::Whisky_MaxNumConstrainedVars	= 17
# Whisky::Whisky_MaxNumSandRVars	= 0


###++++++++++++++++++++++++++++++++++++++++
###   EOS 
###++++++++++++++++++++++++++++++++++++++++

Whisky::whisky_eos_type		= "General"
Whisky::whisky_eos_table	= "Ideal_Fluid"


###++++++++++++++++++++++++++++++++++++++++

EOS_Polytrope::eos_gamma			= 2.0
EOS_Polytrope::eos_k				= 100.0

EOS_Ideal_Fluid::eos_ideal_fluid_gamma		= 2.0

###++++++++++++++++++++++++++++++++++++++++
###   Whisky atmosphere parameters
###++++++++++++++++++++++++++++++++++++++++

Whisky::rho_rel_min			= 1.0e-10
Whisky::Whisky_atmo_tolerance		= 0.0001


###============================================================###
###   ANALYSIS
###============================================================###

###++++++++++++++++++++++++++++++++++++++++
###   Whisky_Analysis
###++++++++++++++++++++++++++++++++++++++++

Whisky_Analysis::compute_angular_momentum		= "yes"
Whisky_Analysis::angular_momentum_nr			= 1
Whisky_Analysis::reference_radius_angular_momentum[0]	= 100.0
Whisky_Analysis::compute_specific_angular_momentum	= "yes"

Whisky_Analysis::compute_masses				= "yes"
Whisky_Analysis::masses_nr 				= 1
Whisky_Analysis::reference_radius_mass[0]		= 100.0

Whisky_Analysis::compute_vorticity			= "no"


###============================================================###
###   Numerical Dissipation 
###============================================================###

Dissipation::order	= 5
Dissipation::epsdis	= 0.1
Dissipation::vars = "
	ML_BSSN::ML_log_confac
	ML_BSSN::ML_metric
	ML_BSSN::ML_trace_curv
	ML_BSSN::ML_curv
	ML_BSSN::ML_Gamma
	ML_BSSN::ML_lapse
	ML_BSSN::ML_shift
	ML_BSSN::ML_dtlapse
	ML_BSSN::ML_dtshift
	"


###============================================================###
###   SphericalSurface
###============================================================###

SphericalSurface::nsurfaces		= 2
SphericalSurface::maxntheta             = 140
SphericalSurface::maxnphi               = 240

SphericalSurface::ntheta      [0]	= 24
SphericalSurface::nphi        [0]	= 96
SphericalSurface::symmetric_z [0]	= "yes"
SphericalSurface::nghoststheta[0]	= 2
SphericalSurface::nghostsphi  [0]	= 2

SphericalSurface::ntheta      [1]	= 24
SphericalSurface::nphi	      [1]	= 96
SphericalSurface::symmetric_z [1]	= "yes"
SphericalSurface::nghoststheta[1]	= 2
SphericalSurface::nghostsphi  [1]	= 2


###============================================================###
###   NaNChecker
###============================================================###

NaNChecker::verbose		= "standard"
NaNChecker::check_every		= 1000
NanChecker::check_after		= 0
NaNChecker::out_NaNmask		= "yes"
NaNChecker::check_vars		= "ML_ADMConstraints::ML_Ham"
NaNChecker::action_if_found	= "terminate"


###============================================================###
###   Output
###============================================================###

Formaline::output_source_subdirectory	= "./cactus-source"

IO::strict_io_parameter_check		= "yes"

TimerReport::out_every			= 1000
TimerReport::out_filename		= "timer_report.txt"

Carpet::output_timers_every		= 1000
CarpetLib::print_timestats_every	= 1000
CarpetLib::print_memstats_every		= 1000


###============================================================###
###   Output ( ASCII )
###============================================================###

IO::out_dir	= "./OUTPUT_DATA/RESULTS/ASCII"

CarpetIOASCII::one_file_per_group	= "no"


IOBasic::outInfo_every		= 10
IOBasic::outInfo_reductions	= "maximum norm2"
IOBasic::outInfo_vars		= "HydroBase::rho ML_ADMConstraints::ML_Ham"


IOScalar::one_file_per_group	= "no"
IOScalar::outScalar_reductions	= "minimum maximum norm1 norm2"
IOScalar::outScalar_criterion	= "divisor"
IOScalar::outScalar_every	= 10
IOScalar::outScalar_vars	= "
	HydroBase::rho{ reductions = 'minimum maximum'}
	HydroBase::vel{ reductions = 'minimum maximum'}
	HydroBase::press{ reductions = 'minimum maximum'}
	HydroBase::eps{ reductions = 'minimum maximum'}
	ADMBase::lapse{ reductions = 'minimum maximum'}
	ML_ADMConstraints::ML_Ham{ reductions = 'norm1 norm2 norm_inf'}
	ML_ADMConstraints::ML_mom{ reductions = 'norm1 norm2 norm_inf'}
	"

IOASCII::out0D_criterion	= "divisor"
IOASCII::out0D_every		= 10
IOASCII::out0D_vars		= "
	Whisky_Analysis::whisky_total_rest_mass
	Whisky_Analysis::whisky_total_angular_momentum
	Whisky_Analysis::whisky_angular_momentum_z
	"

IOASCII::out1D_criterion	= "divisor"
IOASCII::out1D_d		= "no"
IOASCII::out1D_every		= 10
IOASCII::out1D_vars		= "
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
	HydroBase::rho
	HydroBase::vel
	HydroBase::press
	HydroBase::eps
	ADMBase::metric
	ADMBase::curv 
	ADMBase::lapse
	ADMBase::shift 
	"

###============================================================###
###   Output ( HDF5 )
###============================================================###

CarpetIOHDF5::compression_level = 6

CarpetIOHDF5::out1d_dir	= "./OUTPUT_DATA/RESULTS/HDF5"
CarpetIOHDF5::out2d_dir	= "./OUTPUT_DATA/RESULTS/HDF5"
CarpetIOHDF5::out_dir	= "./OUTPUT_DATA/RESULTS/HDF5"

# CarpetIOHDF5::out1d_vars  = 
#       "

CarpetIOHDF5::out2d_vars	= "
	HydroBase::rho
	HydroBase::vel
	ADMBase::lapse
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
	"

CarpetIOHDF5::out_vars	= "
	HydroBase::rho
	ML_ADMConstraints::ML_Ham
	"

CarpetIOHDF5::out1d_every	= -1
CarpetIOHDF5::out2d_every	= -1
CarpetIOHDF5::out_every		= -1


###============================================================###
###   Checkpointing
###============================================================###

CarpetIOHDF5::checkpoint	= "yes"

IO::checkpoint_ID		= "no"
IO::recover			= "autoprobe"
IO::checkpoint_every		= 1000
IO::checkpoint_keep		= 4
IO::checkpoint_dir		= "./OUTPUT_DATA/CHECKPOINT"
IO::recover_dir			= "./OUTPUT_DATA/CHECKPOINT"

Carpet::regrid_during_recovery	= "no"

CarpetIOHDF5::use_grid_structure_from_checkpoint	= "yes"
CarpetIOHDF5::open_one_input_file_at_a_time		= "yes"
CarpetIOHDF5::use_checksums				= "yes"


###============================================================###
###   Wall Time Limit
###============================================================###

ManualTermination::termination_from_file	= "yes"
ManualTermination::termination_file		= "./TERMINATION_File.txt"

ManualTermination::check_file_every		= 1000

ManualTermination::output_remtime_every_minutes	= 60

ManualTermination::on_remaining_walltime	= 10
ManualTermination::max_walltime			= 15

IO::checkpoint_on_terminate			= "yes"


###======================================================###
###======================================================###

