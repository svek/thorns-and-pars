#!/usr/bin/python2

import sys,os
sys.path.append("/home/sven/numrel/ET/pycactuset/PostCactus") # change to where you downloaded PostCactus

import numpy as np
from numpy.linalg import norm
from postcactus.simdir import SimDir
from numpy.linalg import norm

sd = SimDir("./")

fieldname = "gxx"

# assume gauge wave in x direction
omnireader = sd.grid.x

iters = omnireader.get_iters(fieldname)
times = omnireader.get_times(fieldname)
initialdata = omnireader.read(fieldname, iters[0])
finaldata = omnireader.read(fieldname, iters[-1])
dx = initialdata.dx_finest()
L = initialdata.x1() - initialdata.x0() # size of physical domain

assert times[0] == 0.0 and iters[0] == 0, "No restarts, please."
assert np.all(initialdata.dx_finest() == initialdata.dx_coarse()), "Please run a convergence test only with one reflevel"
assert len(dx) == 1, "Use the same grid spacing in any direction"
assert len(initialdata) == 1 and len(finaldata) == 1, "Use only single core, no MPI."
assert len(L) == 1, "I assumed 1D data. Strange."

dim = 3 # Cactus is always 3d
dx, L = dx[0], L[0]
initialdata = initialdata[0].data
finaldata = finaldata[0].data
error = np.abs(finaldata - initialdata) * dx**dim

Nelem = L / dx

print "Cactus-Gaugewave: L = %f, dx = %f, t_final = %f" % (L, dx, times[-1])
print "Nelem = %d, error norm2 = %e, error normInf = %e " % (Nelem, norm(error,2), norm(error,np.inf))


