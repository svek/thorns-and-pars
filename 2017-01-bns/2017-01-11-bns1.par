# The bns1 parfile, v0.1
# 2017, SvenK

# v0.1: Started parfile 2017-01-11
#
# Ingredients are:
# 1) Equal mass (for the time being)
# 2) 45km seperation
# 3) LS220 Lorene ID 0.01MeV
# 4) Evolution with ideal EOS
# 5) grid setup from Federicos elh.par (see email)
#
# Straightforward upgrade potential:
# a) Refluxing
# b) LS220 thermal EOS for evolution

ActiveThorns	= "
	ADMBase
	ADMCoupling
	ADMMacros
	AEILocalInterp
	AHFinderDirect
	BNSTrackerGen
	Boundary
	Carpet
	CarpetInterp
	CarpetIOASCII
	CarpetIOBasic
	CarpetIOHDF5
	CarpetIOScalar
	CarpetMask
	CarpetLib
	CarpetRegrid2
	CarpetReduce
	CarpetSlab
	CartGrid3D
	Constants
	CoordBase
	CoordGauge
	Dissipation
	EOS_Barotropic
	EOS_Thermal
	EOS_Thermal_Idealgas
	EOS_Thermal_Hybrid
	Fortran
	GenericFD
	HydroBase
	HRSCCore
	InitBase
	IOUtil
	LocalInterp2
	LoreneID
	LoopControl
	ML_ADMConstraints
	ML_CCZ4
	ML_CCZ4_Helper
	MoL
	Multipole
	NaNChecker
	NewRad
	PizzaBase
	PizzaIDBase
	PizzaNumUtils
	ReflectionSymmetry
	RotatingSymmetry180
	Slab
	SpaceMask
	SphericalSurface
	StaticConformal
	SymBase
	SystemStatistics
	TerminationTrigger
	THC_Core
	Time
	TimerReport
	TmunuBase
	Volomnia
	WatchDog
	WeylScal4
"


#=============================================================================
# Termination
#=============================================================================

Cactus::terminate	= "never"

TerminationTrigger::max_walltime	= 10 #@WALLTIME_HOURS@ 
TerminationTrigger::on_remaining_walltime	= 30
TerminationTrigger::termination_from_file	= "yes"
TerminationTrigger::create_termination_file	= "yes"
TerminationTrigger::termination_file	= "TERMINATE"
TerminationTrigger::output_remtime_every_minutes	= 60

TimerReport::out_every	= 0
TimerReport::output_all_timers_readable	= "yes"


#=============================================================================
# Grid
#=============================================================================

Grid::avoid_origin	= "no"
Grid::domain	= "full"
Grid::type	= "coordbase"

ReflectionSymmetry::reflection_x	= "no"
ReflectionSymmetry::reflection_y	= "no"
ReflectionSymmetry::reflection_z	= "yes"
ReflectionSymmetry::avoid_origin_x	= "no"
ReflectionSymmetry::avoid_origin_y	= "no"
ReflectionSymmetry::avoid_origin_z	= "no"

CoordBase::xmin	=    0
CoordBase::xmax	=  512
CoordBase::ymin	= -512
CoordBase::ymax	=  512
CoordBase::zmin	=    0
CoordBase::zmax	=  512

CoordBase::spacing	= "numcells"
CoordBase::ncells_x	=    64
CoordBase::ncells_y	=   128
CoordBase::ncells_z	=    64

# $ghosts = 3
Driver::ghost_size	= 3

CoordBase::boundary_size_x_lower	= 3 # $ghosts
CoordBase::boundary_size_x_upper	= 3 # $ghosts
CoordBase::boundary_shiftout_x_lower	= 1
CoordBase::boundary_shiftout_x_upper	= 0
CoordBase::boundary_staggered_x_lower	= "no"
CoordBase::boundary_staggered_x_upper	= "no"

CoordBase::boundary_size_y_lower	= 3 # $ghosts
CoordBase::boundary_size_y_upper	= 3 # $ghosts
CoordBase::boundary_shiftout_y_lower	= 0
CoordBase::boundary_shiftout_y_upper	= 0
CoordBase::boundary_staggered_y_lower	= "no"
CoordBase::boundary_staggered_y_upper	= "no"

CoordBase::boundary_size_z_lower	= 3 # $ghosts
CoordBase::boundary_size_z_upper	= 3 # $ghosts
CoordBase::boundary_shiftout_z_lower	= 1
CoordBase::boundary_shiftout_z_upper	= 0
CoordBase::boundary_staggered_z_lower	= "no"
CoordBase::boundary_staggered_z_upper	= "no"

Carpet::domain_from_coordbase	= "yes"

InitBase::initial_data_setup_method	= "init_some_levels"

Carpet::max_refinement_levels		= 6
Carpet::prolongation_order_space	= 3
Carpet::prolongation_order_time		= 2
Carpet::use_buffer_zones	= "yes"
Carpet::enable_all_storage	= "no"
Carpet::init_fill_timelevels	= "yes"

Carpet::grid_coordinates_filename	= "grid.carpet"

# the grids are from Federico

CarpetRegrid2::num_centres	= 3
CarpetRegrid2::regrid_every	= 64
CarpetRegrid2::ensure_proper_nesting	= "yes"
CarpetRegrid2::freeze_unaligned_levels	= "yes"
CarpetRegrid2::freeze_unaligned_parent_levels	= "yes"
CarpetRegrid2::snap_to_coarse	= "yes"

# ----------- Region 1 --------------------
CarpetRegrid2::active_1	= "yes"
CarpetRegrid2::num_levels_1	= 6
CarpetRegrid2::position_x_1	= +17.5
CarpetRegrid2::radius_1[1]	= 100.0
CarpetRegrid2::radius_1[2]	= 50.0
CarpetRegrid2::radius_1[3]	= 40.0
CarpetRegrid2::radius_1[4]	= 26.0
CarpetRegrid2::radius_1[5]	= 15.0

# ----------- Region 2 --------------------
CarpetRegrid2::active_2	= "yes"
CarpetRegrid2::num_levels_2	= 6
CarpetRegrid2::position_x_2	= -17.5
CarpetRegrid2::radius_2[1]	= 100.0
CarpetRegrid2::radius_2[2]	= 50.0
CarpetRegrid2::radius_2[3]	= 40.0
CarpetRegrid2::radius_2[4]	= 26.0
CarpetRegrid2::radius_2[5]	= 15.0

# ----------- Region 3 --------------------
CarpetRegrid2::active_3	= "yes"
CarpetRegrid2::num_levels_3	= 4
CarpetRegrid2::position_x_3	= 0
CarpetRegrid2::radius_3[1]	= 240.0
CarpetRegrid2::radius_3[2]	= 120.0
CarpetRegrid2::radius_3[3]	= 60.0
CarpetRegrid2::radius_3[4]	= 40.0
CarpetRegrid2::radius_3[5]	= 30.0

CarpetRegrid2::symmetry_rotating180	= "yes"
CarpetRegrid2::verbose			= "no"

BNSTrackerGen::sym_pi			= "yes"
BNSTrackerGen::analysis_reflevel	= 5
BNSTrackerGen::analyze_every		= 64
BNSTrackerGen::merge_separation		= 7.4
BNSTrackerGen::collapse_separation	= -1
BNSTrackerGen::add_levels_post_merge	= 2
BNSTrackerGen::add_levels_post_collapse	= 0

NaNChecker::check_every = 4096
NaNChecker::action_if_found = "terminate"
NaNChecker::check_vars = "
    HydroBase::rho
    HydroBase::vel
    HydroBase::w_lorentz
    THC_Core::densxn
    THC_Core::densxp
    THC_Core::scon
    THC_Core::tau
    THC_Core::volform
"


#=============================================================================
# Time integration (standard)
#=============================================================================

MoL::ode_method			= "RK3"
MoL::MoL_Intermediate_Steps	= 3
MoL::MoL_Num_Scratch_Levels	= 0
MoL::verbose			= "normal"

HydroBase::timelevels		= 3

Time::timestep_method		= "courant_static"
Time::dtfac			= 0.15


#=============================================================================
# Initial data (mostly from Luke)
#=============================================================================

ADMBase::initial_data		= "LoreneBNS"
ADMBase::initial_lapse		= "LoreneBNS"
ADMBase::initial_shift		= "zero"
ADMBase::initial_dtlapse	= "zero"
ADMBase::initial_dtshift	= "zero"

HydroBase::initial_hydro	= "LoreneBNS"
HydroBase::initial_entropy	= "THCode"
HydroBase::initial_temperature	= "LoreneBNS"
HydroBase::initial_Y_e		= "LoreneBNS"
HydroBase::initial_Abar		= "zero"

# PATHS: Using still Lukes files. Exchanged the SFHo -> LS220.

# File describing a one-parametric EOS in Pizza format. Used only for initial data.
#PizzaIDBase::eos_file		= "/home/hpc/pr62do/di29huy2/work/initial_data/eos/SFHo/0.01MeVbeta/eos_SFHo_adb.pizza"
PizzaIDBase::eos_file		= "/home/hpc/pr62do/di29huy2/work/initial_data/eos/LS220/0.01MeVbeta/LS220B0.pizza"
#LoreneID::lorene_bns_file	= "/home/hpc/pr62do/di29huy2/work/initial_data/equal_mass_SFHO_45km/m1.35/resu.d"
LoreneID::lorene_bns_file	= "/home/hpc/pr62do/di29huy2/work/initial_data/equal_mass_LS220_45km/m1.35/resu.d"

# Geometric unit system for initial data, specified by length unit.
# use CACTUS units
PizzaBase::length_unit	= 1476.7161818921163

####### PATHS OF THE OTHER FILES:
## LUKE: /home/hpc/pr62do/di29huy2/sommerville/par/nucleo_runs/bns_sfho_em_1.35.par
## FEDERICO: elh.par (email)



#=============================================================================
# Templated hydrodynamics code
#=============================================================================

TmunuBase::prolongation_type = "none"
TmunuBase::stress_energy_storage = "yes"
TmunuBase::stress_energy_at_RHS = "yes"
TmunuBase::support_old_CalcTmunu_mechanism = "no"

HydroBase::evolution_method	= "THCode"
# default is "none" = no evolution.
#HydroBase::entropy_evolution_method = "THCode"
#HydroBase::temperature_evolution_method = "THCode"

HRSCCore::scheme	= "FD"
#HRSCCore::artvisc	= "yes"
HRSCCore::pplim		= "yes"
HRSCCore::reconstruction= "MP5"
HRSCCore::flux_split	= "LLF"
HRSCCore::system_split	= "components"
#HRSCCore::store_max_wave_speed	= "no"

THC_Core::eos_type	= "ideal"
THC_Core::bc_type	= "none"
THC_Core::atmo_rho	= 1e-16
THC_Core::atmo_eps	= 1e-8 #How do I set this?
THC_Core::atmo_tolerance= 0.0
#THC_Core::atmo_I_know_what_I_am_doing	= "yes" # actually, no.
THC_Core::c2a_BH_alp	= 0.15
THC_Core::c2a_rho_strict= 2e-5
THC_Core::c2a_set_to_nan_on_failure = "no"
THC_Core::c2a_fix_conservatives	= "no"

# Federico hat hybrid here. Strange.
#EOS_Thermal::evol_eos_name	= "Hybrid"
#EOS_Thermal_Hybrid::eos_cold_file	= "@BASEDIR@/ID/codecomp/@EOS@/EOS.pizza"
#EOS_Thermal_Hybrid::gamma_thermal	= 1.75
#EOS_Thermal_Hybrid::eps_max	= 10.0
#EOS_Thermal_Hybrid::eps_min	= -1
#EOS_Thermal_Hybrid::Ye_max	= 2

# I stick to THC_Core::eos_type.
EOS_THermal::evol_eos_name	= "IdealGas"
EOS_Thermal_IdealGas::index_n	= 1
EOS_Thermal_IdealGas::eps_max 	= 2



#=============================================================================
# Spacetime evolution: CCZ4
#=============================================================================

ADMBase::evolution_method		= "ML_CCZ4"
ADMBase::lapse_evolution_method		= "ML_CCZ4"
ADMBase::shift_evolution_method		= "ML_CCZ4"
ADMBase::dtlapse_evolution_method	= "ML_CCZ4"
ADMBase::dtshift_evolution_method	= "ML_CCZ4"

ML_CCZ4::timelevels	= 3

ML_CCZ4::harmonicN	= 1.0 # 1+log
ML_CCZ4::harmonicF	= 2.0 # 1+log
ML_CCZ4::BetaDriver	= 0.4 # ~1/M (\eta)
ML_CCZ4::advectLapse	= 1
ML_CCZ4::advectShift	= 1
ML_CCZ4::evolveA	= 0
ML_CCZ4::evolveB	= 1
ML_CCZ4::shiftGammaCoeff	= 0.75
ML_CCZ4::shiftFormulation	= 0 # Gamma driver
ML_CCZ4::fixAdvectionTerms	= 1

ML_CCZ4::dampk1		= 0.05
ML_CCZ4::dampk2		= 0.0
ML_CCZ4::GammaShift	= 0.5

ML_CCZ4::MinimumLapse		= 1.0e-8
ML_CCZ4::conformalMethod	= 1 # 1 for W
ML_CCZ4::dt_lapse_shift_method	= "noLapseShiftAdvection"

ML_CCZ4::initial_boundary_condition	= "extrapolate-gammas"
ML_CCZ4::rhs_boundary_condition		= "NewRad"
Boundary::radpower			= 2

ML_CCZ4::ML_log_confac_bound	= "none"
ML_CCZ4::ML_metric_bound	= "none"
ML_CCZ4::ML_Gamma_bound		= "none"
ML_CCZ4::ML_trace_curv_bound	= "none"
ML_CCZ4::ML_curv_bound		= "none"
ML_CCZ4::ML_lapse_bound		= "none"
ML_CCZ4::ML_dtlapse_bound	= "none"
ML_CCZ4::ML_shift_bound		= "none"
ML_CCZ4::ML_dtshift_bound	= "none"

ML_CCZ4::fdOrder	= 4
THC_Core::fd_order	= 4

Dissipation::order	= 5
Dissipation::epsdis	= 0.2
Dissipation::vars	= "
	ML_CCZ4::ML_log_confac
	ML_CCZ4::ML_metric
	ML_CCZ4::ML_trace_curv
	ML_CCZ4::ML_curv
	ML_CCZ4::ML_Gamma
	ML_CCZ4::ML_lapse
	ML_CCZ4::ML_shift
	ML_CCZ4::ML_dtlapse
	ML_CCZ4::ML_dtshift
	ML_CCZ4::ML_Theta
"


#=============================================================================
# Analysis (Federico's, haven't touched anything)
#=============================================================================

Volomnia::symm_weight	= 4

#------WeylScal4-----------------------

WeylScal4::fdOrder	= 4
WeylScal4::verbose	= 0

Multipole::nradii	= 4
Multipole::out_every	= 64

Multipole::radius[0]	= 200
Multipole::radius[1]	= 300
Multipole::radius[2]	= 400
Multipole::radius[3]	= 450
Multipole::ntheta	= 120
Multipole::nphi		= 240

Multipole::variables	= "WeylScal4::Psi4r{sw=-2 cmplx='WeylScal4::Psi4i' name='Psi4'}"
Multipole::l_max	= 4

Multipole::output_hdf5	= "yes"
Multipole::output_ascii	= "yes"

#------SphericalSurface-----------------------

SphericalSurface::nsurfaces	= 1
SphericalSurface::maxntheta	= 140
SphericalSurface::maxnphi	= 240

SphericalSurface::ntheta [0]		= 55
SphericalSurface::nphi [0]		= 96
SphericalSurface::nghoststheta [0]	= 2
SphericalSurface::nghostsphi [0]	= 2

#------AHFinderDirect-----------------------

AHFinderDirect::N_horizons	= 1
AHFinderDirect::find_every	= 64
AHFinderDirect::output_h_every	= 0
AHFinderDirect::max_Newton_iterations__initial	= 50
AHFinderDirect::max_Newton_iterations__subsequent	= 50
AHFinderDirect::max_allowable_Theta_growth_iterations	= 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations	= 10
AHFinderDirect::geometry_interpolator_name	= "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars	= "order=4"
AHFinderDirect::surface_interpolator_name	= "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars	= "order=4"
AHFinderDirect::verbose_level	= "physics details"
AHFinderDirect::move_origins	= "yes"

AHFinderDirect::origin_x[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__x_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__y_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__z_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__radius[1]	= 3.0
AHFinderDirect::which_surface_to_store_info[1]	= 0
AHFinderDirect::set_mask_for_individual_horizon[1]	= "no"
AHFinderDirect::reset_horizon_after_not_finding[1]	= "no"
AHFinderDirect::find_after_individual_time[1]	= 2000.0
AHFinderDirect::max_allowable_horizon_radius[1]	= 5.0

# new parameters suggested by Erik, also for stability at recovery
AHFinderDirect::reshape_while_moving	= "yes"
AHFinderDirect::predict_origin_movement	= "yes"

#--------QuasiLocalMeasures-----------------

#QuasiLocalMeasures::interpolator	= "Lagrange polynomial interpolation"
#QuasiLocalMeasures::interpolator_options= "order=4"
#QuasiLocalMeasures::spatial_order	= 4
#
#QuasiLocalMeasures::num_surfaces	= 1
#QuasiLocalMeasures::surface_index [0]	= 0

#--------CarpetMask--------------------------

CarpetMask::excluded_surface [0]	= 0
CarpetMask::excluded_surface_factor [0]	= 1


#=============================================================================
# Checkpoint
#=============================================================================

CarpetIOHDF5::checkpoint	= "yes"
CarpetIOHDF5::use_reflevels_from_checkpoint	= "yes"

IOUtil::checkpoint_on_terminate	= "yes"
#IOUtil::checkpoint_every_walltime_hours	= 6
IOUtil::checkpoint_every = 4096
IOUtil::checkpoint_keep	= 1
IOUtil::recover	= "autoprobe"
IOUtil::checkpoint_dir	= "../checkpoints"
IOUtil::recover_dir	= "../checkpoints"


#=============================================================================
# Output
#=============================================================================

Cactus::cctk_full_warnings = "yes"
Cactus::highlight_warning_messages	= "yes"

IOUtil::out_dir	= "./data/"
IOUtil::strict_io_parameter_check	= "yes"
IOUtil::parfile_write	= "copy"

CarpetIOScalar::one_file_per_group	= "yes"
CarpetIOScalar::all_reductions_in_one_file = "yes"
CarpetIOASCII::one_file_per_group	= "yes"
CarpetIOHDF5::one_file_per_group	= "yes"
IO::out_single_precision                = "yes"

# STDOUT Info
CarpetIOBasic::outinfo_every = 1
CarpetIOBasic::outInfo_reductions = "minimum maximum"
CarpetIOBasic::outinfo_vars = "
	Carpet::physical_time_per_hour
	HydroBase::rho
	ADMBase::lapse
	SystemStatistics::maxrss_mb
"
# SCALAR data
CarpetIOScalar::outScalar_reductions = "average minimum maximum norm1 norm2 sum"

CarpetIOScalar::outscalar_vars	= "
	admbase::lapse
	admbase::shift
	admbase::curv
	admbase::metric
	hydrobase::rho
	hydrobase::press
	hydrobase::eps
	hydrobase::w_lorentz
	hydrobase::vel
	THC_Core::dens
	THC_Core::tau
	THC_Core::scon
	THC_Core::volform
	THC_Core::bitmask
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom

"

# 0D vars
CarpetIOASCII::out0D_vars	= "
	BNSTrackerGen::bns_positions
	Carpet::physical_time_per_hour
	Volomnia::grid_volume
	Volomnia::cell_volume
	Carpet::timing
"

# 1D vars
IOASCII::out1D_vars      = "
	ADMBase::lapse
	ADMBase::shift
	ADMBase::curv
	ADMBase::metric
	hydrobase::rho
	hydrobase::press
	hydrobase::eps
	hydrobase::w_lorentz
	hydrobase::vel
	THC_Core::scon
	THC_Core::dens
	THC_Core::tau
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
"

# 1D hdf5 for rug
IOHDF5::out1D_vars = "
	ADMBase::lapse
	ADMBase::shift
	ADMBase::curv
	ADMBase::metric
	hydrobase::rho
	hydrobase::press
	hydrobase::eps
	hydrobase::w_lorentz
	hydrobase::vel
	THC_Core::scon
	THC_Core::dens
	THC_Core::tau
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
"

# 2D hdf5 only
IOHDF5::out2D_vars = "
	ADMBase::lapse
	ADMBase::shift
	ADMBase::curv
	ADMBase::metric
        HydroBase::eps
        HydroBase::press
        HydroBase::rho
        HydroBase::vel
        HydroBase::w_lorentz
        HydroBase::entropy
        HydroBase::temperature
        HydroBase::Y_e
	THC_Core::dens
	THC_Core::scon
	THC_Core::tau
	THC_Core::csound
	THC_Core::volform
	ML_ADMConstraints::ML_Ham
	ML_ADMConstraints::ML_mom
"

IOHDF5::out2d_xy	= "yes"
IOHDF5::out2d_xz	= "no"
IOHDF5::out2d_yz	= "yes"

# 3D hdf5
CarpetIOHDF5::out_vars          = "
	HydroBase::rho
	HydroBase::eps
"

CarpetIOScalar::outscalar_every	= 128
CarpetIOASCII::out0d_every	= 128
CarpetIOHDF5::out1d_every	= 256
CarpetIOHDF5::out2d_every	= 1024
CarpetIOHDF5::out3d_every	= 8192




