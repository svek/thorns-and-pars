#!/usr/bin/env python

# Geometric base units in SI units
M_sol = 1.988e30     # kg
R_sol = 1.48 * 1000  # m

# Density factor from Geometric->SI
dens = M_sol / R_sol**3

print dens

