#!/usr/bin/python

from math import exp

rho_nuc = 1.66e17 # kg/m^3
fm = 1e-15 # m
n_nuc = 0.1 * fm**(-3) # m^{-3}

mB = rho_nuc / n_nuc # kg

c = 3e8 # m/s

rhoCC = rho_nuc * c**2 # kg m^{-1} s^{-2} = Pressure

# kappa as given in Lorene
k0 = 0.05 # "in units rho_nuc * c**2 / n_nuc**Gamma"
gamma = 2.

# the polytropic EOS as stated in LORENE
p = lambda n: k0 * ( n / n_nuc )**gamma # in units of rhoCC

H_c = 0.225 # "in c^2 units"
n_c = (gamma-1)/gamma * (mB * c**2)/(k0 * (rhoCC/n_nuc**gamma)) * (exp(H_c)-1)**(1/(gamma-1))

# pressure at center
p_c = p(n_c)

print "Computed central pressure as %f rho_nuc c^2, like LORENE" % p_c

