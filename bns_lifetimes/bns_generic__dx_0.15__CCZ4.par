# Project: BNS Lifetimes
# Purpose: Run a BNS with any EOS
# Coming from: A unification of {ls220,dd2,sfho} eos for different masses.
# Parameters:
#   EOS:  Should be like eos  -> put into filenames, therefore case sensitive.
#   MASS: Should be like 1.35
# Revised:    2018-02-18
# Started at: 2017-12-06, 13:56 by Sven
# Started at: 2017-05-01, 09:48 by Sven on SuperMUC
# Stored in: BNS Lifetimes repository

ActiveThorns = "
    ADMBase
    ADMCoupling
    ADMMass
    ADMMacros
    AEILocalInterp
    AHFinderDirect
    BNSAnalysis
    BNSTrackerGen
    Boundary
    Carpet
    CarpetInterp
    CarpetIOASCII
    CarpetIOBasic
    CarpetIOHDF5
    CarpetIOScalar
    CarpetMask
    CarpetLib
    CarpetRegrid2
    CarpetReduce
    CarpetSlab
    CartGrid3D
    Constants
    Composition
    CoordBase
    CoordGauge
    Dissipation
    Formaline
    EOS_Barotropic
    EOS_Thermal
    EOS_Thermal_Extable
    EOS_Thermal_Table3d
    Fortran
    GenericFD
    HydroBase
    HRSCCore
    ID_Switch_EOS
    InitBase
    IOUtil
    LocalInterp2
    LoreneID
    LoopControl
    ML_ADMConstraints
    ML_CCZ4
    ML_CCZ4_Helper
    MoL
    Multipole
    NaNChecker
    NewRad
    PizzaBase
    PizzaIDBase
    PizzaNumUtils
    ReflectionSymmetry
    Slab
    SpaceMask
    SphericalSurface
    SphericalHarmonicDecomp
    StaticConformal
		SystemStatistics
    SymBase
    TerminationTrigger
    THC_Core
		THC_HydroExcision
    Time
    TimerReport
    TmunuBase
    Volomnia
    WatchDog
    WeylScal4
"


Cactus::terminate			= "time"
Cactus::cctk_final_time		= 1000000
#Cactus::terminate    = "iteration"
#Cactus::cctk_itlast  = 274000
Formaline::output_source_subdirectory		= "../../cactus-source"

TerminationTrigger::max_walltime = @WALLTIME_HOURS@
TerminationTrigger::on_remaining_walltime	= 30
TerminationTrigger::create_termination_file	= "yes"
TerminationTrigger::termination_from_file	= "yes"
TerminationTrigger::termination_file		= "../TERMINATE"

TimerReport::output_all_timers_readable		= "yes"

# =============================================================================
# Grid
# =============================================================================
Grid::avoid_origin			= "no"
Grid::domain			= "full"
Grid::type			= "coordbase"

ReflectionSymmetry::reflection_x		= "no"
ReflectionSymmetry::reflection_y		= "no"
ReflectionSymmetry::reflection_z		= "yes"
ReflectionSymmetry::avoid_origin_x		= "yes"
ReflectionSymmetry::avoid_origin_y		= "yes"
ReflectionSymmetry::avoid_origin_z		= "yes"

Volomnia::symm_weight		= 2

CoordBase::xmin			= -504
CoordBase::xmax			=  504
CoordBase::ymin			= -504
CoordBase::ymax			=  504
CoordBase::zmin			=  0
CoordBase::zmax			=  504

CoordBase::spacing			= "numcells"
CoordBase::ncells_x			= 210
CoordBase::ncells_y			= 210
CoordBase::ncells_z			= 105

CoordBase::boundary_size_x_lower		= 3
CoordBase::boundary_size_x_upper		= 3
CoordBase::boundary_shiftout_x_lower		= 0
CoordBase::boundary_shiftout_x_upper		= 0
CoordBase::boundary_staggered_x_lower		= "yes"
CoordBase::boundary_staggered_x_upper		= "yes"

CoordBase::boundary_size_y_lower		= 3
CoordBase::boundary_size_y_upper		= 3
CoordBase::boundary_shiftout_y_lower		= 0
CoordBase::boundary_shiftout_y_upper		= 0
CoordBase::boundary_staggered_y_lower		= "yes"
CoordBase::boundary_staggered_y_upper		= "yes"

CoordBase::boundary_size_z_lower		= 3
CoordBase::boundary_size_z_upper		= 3
CoordBase::boundary_shiftout_z_lower		= 0
CoordBase::boundary_shiftout_z_upper		= 0
CoordBase::boundary_staggered_z_lower		= "yes"
CoordBase::boundary_staggered_z_upper		= "yes"

Driver::ghost_size			= 3
Driver::ghost_size_x		= 3
Driver::ghost_size_y		= 3
Driver::ghost_size_z		= 3

Carpet::refinement_centering		= "cell"
Carpet::domain_from_coordbase		= "yes"

InitBase::initial_data_setup_method		= "init_some_levels"

# See https://trac.einsteintoolkit.org/ticket/1512
LoopControl::settle_after_iteration		= 0

Carpet::max_refinement_levels		= 7
Carpet::prolongation_order_space		= 3
Carpet::prolongation_order_time		= 2
Carpet::use_buffer_zones		= "yes"
Carpet::enable_all_storage		= "no"
Carpet::init_fill_timelevels		= "yes"

Carpet::grid_coordinates_filename		= "grid.carpet"

CarpetLib::poison_new_memory		= "yes"

CarpetRegrid2::num_centres		= 3
CarpetRegrid2::regrid_every		= 64
CarpetRegrid2::snap_to_coarse		= "yes"
CarpetRegrid2::freeze_unaligned_levels		= "yes"
CarpetRegrid2::freeze_unaligned_parent_levels	= "yes"

# ----------- Region 1 --------------------
CarpetRegrid2::active_1		= "yes"
CarpetRegrid2::num_levels_1		= 6
CarpetRegrid2::position_x_1		= 16.0
CarpetRegrid2::radius_1[1]		= 192.0
CarpetRegrid2::radius_1[2]		= 96.0
CarpetRegrid2::radius_1[3]		= 48.0
CarpetRegrid2::radius_1[4]		= 24.0
CarpetRegrid2::radius_1[5]		= 16.0

# ----------- Region 2 --------------------
CarpetRegrid2::active_2		= "yes"
CarpetRegrid2::num_levels_2		= 6
CarpetRegrid2::position_x_2		= -16.0
CarpetRegrid2::radius_2[1]		= 192.0
CarpetRegrid2::radius_2[2]		= 96.0
CarpetRegrid2::radius_2[3]		= 48.0
CarpetRegrid2::radius_2[4]		= 24.0
CarpetRegrid2::radius_2[5]		= 16.0

# ----------- Region 3 --------------------
CarpetRegrid2::active_3		= "yes"
CarpetRegrid2::num_levels_3		= 3
CarpetRegrid2::position_x_3		= 0

CarpetRegrid2::radius_3[1]		= 256.0
CarpetRegrid2::radius_3[2]		= 128.0

CarpetRegrid2::radius_3[3]		= 64.0
CarpetRegrid2::radius_3[4]		= 32.0

CarpetRegrid2::radius_x_3[5]		= 24.0
CarpetRegrid2::radius_y_3[5]		= 24.0
CarpetRegrid2::radius_z_3[5]		= 16.0

CarpetRegrid2::radius_3[6]		= 8.0
# -----------------------------------------

BNSTrackerGen::sym_pi		= "no"
BNSTrackerGen::analysis_reflevel		= 5
BNSTrackerGen::analyze_every		= 64
BNSTrackerGen::merge_separation		= 1.5
BNSTrackerGen::collapse_separation		= -1
BNSTrackerGen::add_levels_post_merge		= 3
BNSTrackerGen::add_levels_post_collapse		= 0

CarpetRegrid2::symmetry_rotating180		= "no"
CarpetRegrid2::verbose		= "no"

NaNChecker::check_every		= 2048
NaNChecker::check_vars		= "
    HydroBase::rho
    HydroBase::vel
    HydroBase::w_lorentz
    THC_Core::densxn
    THC_Core::densxp
    THC_Core::scon
    THC_Core::tau
    THC_Core::volform
"
NaNChecker::action_if_found		= "terminate"

# =============================================================================
# Time integration
# =============================================================================
Carpet::num_integrator_substeps		= 3

MoL::ode_method			= "RK3"
MoL::MoL_Intermediate_Steps		= 3
MoL::MoL_Num_Scratch_Levels		= 0
MoL::verbose			= "register"

HydroBase::timelevels		= 3

Time::timestep_method		= "courant_static"
Time::dtfac			= 0.15

# =============================================================================
# Initial data
# =============================================================================
ADMBase::initial_data		= "LoreneBNS"
ADMBase::initial_lapse		= "LoreneBNS"
ADMBase::initial_shift		= "zero"
ADMBase::initial_dtlapse		= "zero"
ADMBase::initial_dtshift		= "zero"
HydroBase::initial_hydro		= "LoreneBNS"
HydroBase::initial_entropy		= "THCode"
HydroBase::initial_temperature		= "LoreneBNS"
HydroBase::initial_Y_e		= "LoreneBNS"
HydroBase::initial_Abar		= "zero"

# File describing a one-parametric EOS in Pizza format. Used only for initial data.
PizzaIDBase::eos_file		= "/home/hpc/pr27ju/di25cux4/work/id/bns/bnslt-eos-db/@EOS@/@EOS@.pizza"

# LORENE BNS file
LoreneID::lorene_bns_file                                   = "/home/hpc/pr27ju/di25cux4/work/id/bns/bnslt-eos-db/@EOS@/m@MASS@/resu.d"
LoreneID::link_dummy_eos                                    = "yes"
LoreneID::rho_cut = 1.e-5
# Geometric unit system for initial data, specified by length unit.
# use CACTUS units
PizzaBase::length_unit		= 1476.7161818921163


# Switch EOS
ID_Switch_EOS::sync_eps_temp		= "yes"
ID_Switch_EOS::temp_from_eps		= "no"
ID_Switch_EOS::limit_efrac		= "yes"


# =============================================================================
# Templated hydrodynamics code
# =============================================================================
HydroBase::evolution_method		= "THCode"
HydroBase::temperature_evolution_method		= "THCode"
HydroBase::entropy_evolution_method		= "THCode"
HydroBase::Y_e_evolution_method		= "THCode"
#HydroBase::Abar_evolution_method		= "Composition"

THC_Core::eos_type			= "nuclear"
THC_Core::physics			= "GRHD"

TmunuBase::prolongation_type		= "none"
TmunuBase::stress_energy_storage		= "yes"
TmunuBase::stress_energy_at_RHS		= "yes"
TmunuBase::support_old_CalcTmunu_mechanism	= "no"

THC_Core::bc_type			= "none"

HRSCCore::scheme			= "FV" #  FD is bad. was FV, FD should give ~5th order
HRSCCore::pplim			= "yes"
HRSCCore::reconstruction		= "MP5"
HRSCCore::riemann_solver		= "HLLE"
HRSCCore::flux_split		= "LLF"
HRSCCore::system_split		= "components"
HRSCCore::speed_eps			= 0.05

THC_HydroExcision::excision_surface		= 0
THC_HydroExcision::excision_margin		= 0.9
THC_Core::atmo_rho			= 1e-14
THC_Core::atmo_tolerance		= 0.0
THC_Core::atmo_temperature		= 0.02

THC_Core::c2a_BH_alp		= 0.15
THC_Core::c2a_rho_strict		= 2e-5
THC_Core::c2a_set_to_nan_on_failure		= "no"
THC_Core::c2a_fix_conservatives		= "yes"
THC_Core::c2a_kill_on_failure		= "no"

EOS_Thermal::evol_eos_name		= "Extable"
EOS_Thermal_Extable::rho_max		= 1e10
EOS_Thermal_Extable::temp_max		= 1000
EOS_Thermal_Extable::extend_ye		= "yes"

EOS_Thermal_Table3d::eos_db_loc		= "/home/hpc/pr27ju/di25cux4/work/id/bns/bnslt-eos-db"
EOS_Thermal_Table3d::eos_folder		= "@EOS@"
EOS_Thermal_Table3d::eos_filename	= "eos_thermal_table.h5"


# =============================================================================
# Spacetime evolution
ADMBase::evolution_method		= "ML_CCZ4"
ADMBase::lapse_evolution_method		= "ML_CCZ4"
ADMBase::shift_evolution_method		= "ML_CCZ4"
ADMBase::dtlapse_evolution_method		= "ML_CCZ4"
ADMBase::dtshift_evolution_method		= "ML_CCZ4"
# =============================================================================
ML_CCZ4::GammaShift                     = 0.5
ML_CCZ4::dampk1                 = 0.05  # 0.05 reported by Elias. ~ [0.02, 0.1]/M
ML_CCZ4::dampk2                 = 0.0
ML_CCZ4::harmonicN                      = 1.0    # 1+log
ML_CCZ4::harmonicF                      = 2.0    # 1+log
ML_CCZ4::ShiftGammaCoeff                = 0.75
ML_CCZ4::AlphaDriver            = 0.0
ML_CCZ4::BetaDriver                     = 0.3    # ~ [1, 2] / M (\eta)

ML_CCZ4::advectLapse            = 1
ML_CCZ4::advectShift            = 1

ML_CCZ4::MinimumLapse           = 1.0e-8
ML_CCZ4::conformalMethod                = 1      # 1 for W

ML_CCZ4::initial_boundary_condition          = "extrapolate-gammas"
ML_CCZ4::rhs_boundary_condition              = "NewRad"
Boundary::radpower			= 2

ML_CCZ4::ML_log_confac_bound            = "none"
ML_CCZ4::ML_metric_bound                = "none"
ML_CCZ4::ML_Gamma_bound         = "none"
ML_CCZ4::ML_trace_curv_bound            = "none"
ML_CCZ4::ML_curv_bound          = "none"
ML_CCZ4::ML_lapse_bound         = "none"
ML_CCZ4::ML_dtlapse_bound               = "none"
ML_CCZ4::ML_shift_bound         = "none"
ML_CCZ4::ML_dtshift_bound               = "none"
ML_CCZ4::ML_Theta_bound         = "none"

ML_CCZ4::fdOrder                        = 4
THC_Core::fd_order			= 4

Dissipation::order			= 5
Dissipation::epsdis                     = 0.2
Dissipation::vars			= "
ML_CCZ4::ML_log_confac
ML_CCZ4::ML_metric
ML_CCZ4::ML_trace_curv
ML_CCZ4::ML_curv
ML_CCZ4::ML_Gamma
ML_CCZ4::ML_lapse
ML_CCZ4::ML_shift
ML_CCZ4::ML_dtlapse
ML_CCZ4::ML_dtshift
ML_CCZ4::ML_Theta
"

# =============================================================================
# Analysis
# =============================================================================
ADMMass::ADMMass_volume_radius[0]		= 450.0

#----- Wave Extraction using "WeylScal4" and "Multipole" --------
WeylScal4::offset			= 1.0e-8
WeylScal4::fd_order			= "4th" # old style
# WeylScal4::fdOrder		= 4  # New feature

WeylScal4::verbose			= 0

Multipole::nradii			= 7

Multipole::radius[0]		= 200
Multipole::radius[1]		= 250
Multipole::radius[2]		= 300
Multipole::radius[3]		= 350
Multipole::radius[4]		= 400
Multipole::radius[5]		= 450
Multipole::radius[6]		= 500
Multipole::ntheta			= 120
Multipole::nphi			= 240

Multipole::variables		= "
WeylScal4::Psi4r{sw=-2 cmplx='WeylScal4::Psi4i' name='Psi4'}"
Multipole::l_max			= 4

#------SphericalSurface-----------------------
SphericalSurface::nsurfaces		= 11
SphericalSurface::maxntheta		= 140
SphericalSurface::maxnphi		= 240

SphericalSurface::ntheta	[0]	= 55
SphericalSurface::nphi	[0]	= 96
SphericalSurface::nghoststheta	[0]	= 2
SphericalSurface::nghostsphi	[0]	= 2

SphericalSurface::ntheta	[1]	= 55
SphericalSurface::nphi	[1]	= 96
SphericalSurface::nghoststheta	[1]	= 2
SphericalSurface::nghostsphi	[1]	= 2

SphericalSurface::ntheta	[2]	= 55
SphericalSurface::nphi	[2]	= 96
SphericalSurface::nghoststheta	[2]	= 2
SphericalSurface::nghostsphi	[2]	= 2
SphericalSurface::set_spherical	[2]	= "yes"
SphericalSurface::radius	[2]	= 100

SphericalSurface::ntheta	[3]	= 55
SphericalSurface::nphi	[3]	= 96
SphericalSurface::nghoststheta	[3]	= 2
SphericalSurface::nghostsphi	[3]	= 2
SphericalSurface::set_spherical	[3]	= "yes"
SphericalSurface::radius	[3]	= 150

SphericalSurface::ntheta	[4]	= 55
SphericalSurface::nphi	[4]	= 96
SphericalSurface::nghoststheta	[4]	= 2
SphericalSurface::nghostsphi	[4]	= 2
SphericalSurface::set_spherical	[4]	= "yes"
SphericalSurface::radius	[4]	= 200

SphericalSurface::ntheta	[5]	= 55
SphericalSurface::nphi	[5]	= 96
SphericalSurface::nghoststheta	[5]	= 2
SphericalSurface::nghostsphi	[5]	= 2
SphericalSurface::set_spherical	[5]	= "yes"
SphericalSurface::radius	[5]	= 250

SphericalSurface::ntheta	[6]	= 55
SphericalSurface::nphi	[6]	= 96
SphericalSurface::nghoststheta	[6]	= 2
SphericalSurface::nghostsphi	[6]	= 2
SphericalSurface::set_spherical	[6]	= "yes"
SphericalSurface::radius	[6]	= 300

SphericalSurface::ntheta	[7]	= 55
SphericalSurface::nphi	[7]	= 96
SphericalSurface::nghoststheta	[7]	= 2
SphericalSurface::nghostsphi	[7]	= 2
SphericalSurface::set_spherical	[7]	= "yes"
SphericalSurface::radius	[7]	= 350

SphericalSurface::ntheta	[8]	= 55
SphericalSurface::nphi	[8]	= 96
SphericalSurface::nghoststheta	[8]	= 2
SphericalSurface::nghostsphi	[8]	= 2
SphericalSurface::set_spherical	[8]	= "yes"
SphericalSurface::radius	[8]	= 400

SphericalSurface::ntheta	[9]	= 55
SphericalSurface::nphi	[9]	= 96
SphericalSurface::nghoststheta	[9]	= 2
SphericalSurface::nghostsphi	[9]	= 2
SphericalSurface::set_spherical	[9]	= "yes"
SphericalSurface::radius	[9]	= 450

SphericalSurface::ntheta	[10]	= 55
SphericalSurface::nphi	[10]	= 96
SphericalSurface::nghoststheta	[10]	= 2
SphericalSurface::nghostsphi	[10]	= 2
SphericalSurface::set_spherical	[10]	= "yes"
SphericalSurface::radius	[10]	= 500

#------AHFinderDirect-----------------------
AHFinderDirect::N_horizons		= 1
AHFinderDirect::find_every		= 1024 #4096  # was 128, effectively disables it.
AHFinderDirect::output_h_every		= 0
AHFinderDirect::max_Newton_iterations__initial	= 50
AHFinderDirect::max_Newton_iterations__subsequent	= 50
AHFinderDirect::max_allowable_Theta_growth_iterations	= 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations	= 10
AHFinderDirect::geometry_interpolator_name	= "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars	= "order=4"
AHFinderDirect::surface_interpolator_name	= "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars	= "order=4"
AHFinderDirect::verbose_level		= "physics highlights"
AHFinderDirect::move_origins		= "yes"

AHFinderDirect::origin_x[1]		= 0.0
AHFinderDirect::initial_guess__coord_sphere__x_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__y_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__z_center[1]	= 0.0
AHFinderDirect::initial_guess__coord_sphere__radius[1]	= 3.0
AHFinderDirect::which_surface_to_store_info[1]	= 0
AHFinderDirect::set_mask_for_individual_horizon[1]	= "no"
AHFinderDirect::reset_horizon_after_not_finding[1]	= "no"
AHFinderDirect::find_after_individual_time[1]	= 100.0
AHFinderDirect::max_allowable_horizon_radius[1]	= 15.0


# new parameters suggested by Erik, also for stability at recovery
AHFinderDirect::reshape_while_moving		= "yes"
AHFinderDirect::predict_origin_movement		= "yes"


#--------CarpetMask--------------------------
CarpetMask::excluded_surface	[0]	= 0
CarpetMask::excluded_surface_factor	[0]	= 1
CarpetMask::excluded_surface	[1]	= 1
CarpetMask::excluded_surface_factor	[1]	= 1

# =============================================================================
# Checkpoint
# =============================================================================
CarpetIOHDF5::checkpoint		= "yes"
CarpetIOHDF5::use_reflevels_from_checkpoint	= "yes"

IOUtil::checkpoint_on_terminate		= "yes"
IOUtil::checkpoint_every		= 4096
IOUtil::checkpoint_keep		= 1
IOUtil::recover			= "autoprobe"
IOUtil::checkpoint_dir		= "../checkpoint"
IOUtil::recover_dir			= "../checkpoint"

# =============================================================================
# Output
# =============================================================================
IOUtil::out_dir			= "./data/"
IOUtil::strict_io_parameter_check		= "yes"
IOUtil::parfile_write		= "copy"

CarpetIOBasic::outinfo_vars		= "
    Carpet::physical_time_per_hour
    ADMBase::lapse
    HydroBase::rho
 		SystemStatistics::maxrss_mb
		SystemStatistics::swap_used_mb
"

CarpetIOScalar::outScalar_reductions		= "
    minimum maximum norm_inf norm1 norm2
"
CarpetIOScalar::outscalar_vars		= "
    ADMBase::lapse
    HydroBase::rho
    ML_ADMConstraints::ML_Ham
    ML_ADMConstraints::ML_mom
    THC_Core::dens
    HydroBase::temperature
    HydroBase::rho
"

CarpetIOASCII::out0D_vars		= "
    ADMMass::ADMMass_Masses
    BNSTrackerGen::bns_positions
    Carpet::timing
"

CarpetIOASCII::out1d_vars		= "
    ADMBase::lapse
    HydroBase::rho
    ML_ADMConstraints::ML_Ham
    ML_ADMConstraints::ML_mom
    THC_Core::dens
    HydroBase::temperature
    HydroBase::rho
"

CarpetIOHDF5::out2d_vars		= "
    ADMBase::lapse
    ADMBase::shift
    ADMBase::metric
    HydroBase::vel
    HydroBase::w_lorentz
    HydroBase::eps
    HydroBase::press
    HydroBase::entropy
    HydroBase::temperature
    HydroBase::Y_e
    HydroBase::rho
    ML_ADMConstraints::ML_Ham
    ML_ADMConstraints::ML_mom
"


CarpetIOScalar::one_file_per_group		= "yes"
CarpetIOScalar::all_reductions_in_one_file	= "yes"
CarpetIOASCII::one_file_per_group		= "yes"
IO::out_single_precision		= "yes"

CarpetIOBasic::outinfo_every		= 128
CarpetIOScalar::outscalar_criterion	= "divisor"
CarpetIOASCII::out0d_criterion		= "divisor"
CarpetIOASCII::out0d_every		= 128
CarpetIOASCII::out1d_every		= 512
CarpetIOScalar::outscalar_every		= 128
CarpetIOHDF5::out2d_every		= 1024
CarpetIOHDF5::out_every		= -1

Multipole::out_every		= 512  # was 128

